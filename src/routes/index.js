import express from 'express';
import config from '../config';
import middleware from '../middleware'
import initializeDb from '../db';
import user from '../controllers/userController';
import uploads from '../controllers/uploadsController';
import chats from '../controllers/chatsController';

let router = express();

// connect to db
initializeDb(db => {
    router.use(middleware({ config, db }));


    // api routes v1
    router.use('/account', user({ config, db }));  // Routes for Creating, Reading, Updating and Deleting a User

    router.use('/uploads', uploads({ config, db })); // Routes for all file uploads

    router.use('/chat', chats({ config, db })); //Routes for all chats
});

export default router;