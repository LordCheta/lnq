export default {
    "port": 4005,
    "mongoUrl": "mongodb://localhost:27017/lnq",
    "bodyLimit": "100kb",
    "secret": "theamazinglnq"
}