import http from 'http';
import express from 'express';
import fileUpload from 'express-fileupload';
import jwt from 'jsonwebtoken';

import config from './config';
import routes from './routes';

// All socket code
import chat from "./chat/chat";

let app = express();
let server = http.createServer(app);  
let io = require('socket.io')(server);

// defaults
app.use(express.json({
    limit: config.bodyLimit
}));
app.use(fileUpload());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8080");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, PATCH");
    next();
  });

// api routes v1
app.use('/lnqapi/v1', routes);

// Socket.io connection   
app.set('socketio', io); // This would be removed. It's here so the chatsController doesn't throw an error. It exposes the sockect instance to it.

io.on('connection', socket => {
    chat(socket);
});

server.listen(config.port);
console.log(`server running on port ${server.address().port}`);

export default app;