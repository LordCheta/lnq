import { Router } from 'express';
import User from '../models/userModel';
import config from '../config';
// import middleware from '../middleware'
import initializeDb from '../db';

export default async socket => {

    let api = Router();
    console.log('A user connected');

    // try {
    //     let result = await User.find({});
    //     console.log(result);
    // } catch(e) {
    //     console.log(e);
    // }
    
    socket.on('disconnect', () => {
        // console.log('A user disconnected');
    });

    socket.on('chatMessage', (msg) => {
        // socket.broadcast.emit(msg);
        console.log(msg);
        socket.broadcast.emit('resMessage', msg);
    });
}