import mongoose from 'mongoose';
import displayPictureModel from './displayPictureModel';

let Schema = mongoose.Schema;

let User = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    isOnline: {
        type: Boolean,
        default: false
    },
    contacts: [{ type: Schema.Types.ObjectId }],

    profile: {
        displayName: { type: String },
        bio: { type: String }, 
        sex: { type: String },
        website: { type: String },
        phoneNumber: { type: Number },
        birthday: { type: String },
        displayPicture: { type: Schema.Types.ObjectId, ref: displayPictureModel}
    },
    verification: {
        isVerified: { type: Boolean, default: false },
        verifiedAt: { type: Date }
    }
});


module.exports = mongoose.model('User', User);