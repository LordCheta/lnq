import mongoose from "mongoose";
import User from './userModel';


let Schema = mongoose.Schema;

let Message = new Schema({
    sentBy: {
        type: Schema.Types.ObjectId,
        ref: User,
        timeSent: { type: Date }
    },
    recievedBy: {
        type: Schema.Types.ObjectId,
        ref: User,
        timeRecieved: { type: Date }
    }
});

module.exports = mongoose.model('Message', Message);