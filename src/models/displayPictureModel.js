import mongoose from 'mongoose';
import userModel from './userModel';

let Schema = mongoose.Schema;

let displayPicture = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: userModel,
        required: true
    },
    displayPictureUrl: { type: String },
    mimetype: { type: String },
    dateUploaded: { type: Date },
});


module.exports = mongoose.model('displayPicture', displayPicture);