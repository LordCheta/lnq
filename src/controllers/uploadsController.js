import { Router } from 'express';
import displayPicture from '../models/displayPictureModel';
import User from '../models/userModel';
import { validateDisplayPicture } from '../middleware/validators/fileTypeValidators'; //Media type validators
import { verifyToken } from '../middleware/accessToken'

export default ({ config, db}) => {

    let api = Router();

    // '/lnqapi/v1/uploads/uploadDp/:id' - End point to add user image
    //TODO - Use Cloudinary to manage all media asserts

    // 'lnqapi/v1/uploads/uploadDp/:id      // Route for uploading user profile or display picture
    api.post('/uploadDp/:id', async (req, res) => {

        let accessToken = req.body.token || req.query.token || req.headers['x-access-token'];

        if(!accessToken) {
            return res.status(407).json({ message: "No token provided" });
        }

        let isTokenValid = verifyToken(accessToken);
        
        if(isTokenValid === false) {
            return res.status(401).json({ message: "Failed to verify token"});
        }

        if (!req.files) return res.status(400).json({message: 'No Files were uploaded'});

        let mediaFile = req.files.lnqMediaFile;
        let isValid = validateDisplayPicture(mediaFile);

        if(isValid === false) return res.status(400).json({ message: "Please Upload A Valid Filetype" });

        try {
            const user = await User.findById(req.params.id);

            if(user) {
                mediaFile.mv(`./tempMedia/displayPictures/${mediaFile.name}`) // Move the uploaded file to a tempStorage folder on the server
            
                let newDisplayPicture = new displayPicture();
            
                newDisplayPicture.user = req.params.id;
                newDisplayPicture.displayPictureUrl = "on Local Machine";
                newDisplayPicture.dateUploaded = new Date();
                newDisplayPicture.mimetype = mediaFile.mimetype;

                const dp = await newDisplayPicture.save();

                if(dp) {
                    user.profile.displayPicture = dp._id;
                    
                    let done = await user.save();

                    if(done) return res.json({ message: "Display Picture Updated Successfully"  });  
                }
            }          
        } catch (e) {
            res.send(400).json(e);
        }
    });


    //'lnqapi/v1/uploads      // Uploads test route
    api.post('/', async (req, res) => {
        if (!req.files) return res.status(400).json({message: 'No Files were uploaded'});
    
        let mediaFile = req.files.lnqMediaFile;

        console.log(__dirname)

        try {
            const result = await mediaFile.mv(`./${mediaFile.name}`)
            res.json({ message: "Upload Successful" });
        } catch (e) {
            res.json(e)
        }

    });

  


    return api;
}