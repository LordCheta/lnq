import { Router } from 'express';
import User from '../models/userModel';
import displayPictureModel from '../models/displayPictureModel';
import { hashPassword } from '../middleware/passwordMiddleware';
import { doesEmailExist } from '../middleware/validators/emailValidator';
import { sendVerificationMail } from '../middleware/verificationMail';
import { setToken, verifyToken } from '../middleware/accessToken';

export default ({ config, db}) => {

    let api = Router();

    // '/lnqapi/v1/account/verifyEmail/:email' - End point to check if email is already in use [Auth Token not required]

    api.get('/verifyEmail/:email', async (req, res) => {
        const newUserEmail = req.params.email;
        let existingUserEmails = await User.find({email: newUserEmail});
        const emailStatus = doesEmailExist(newUserEmail, existingUserEmails);

        if(emailStatus === true) return res.status(400).json({ message: "Email already in Use"});
            
        res.json({ message: "Email Can Be Used" });
        
    });

    // '/lnqapi/v1/account/register' - End point to create a new user. Email and Password is compulsory [Auth Token not required]

    api.post('/register', async (req, res) => {
        const newUserEmail = req.body.email;
        let existingUserEmails = await User.find({email: newUserEmail});
        const emailStatus = doesEmailExist(newUserEmail, existingUserEmails);

        if(emailStatus === true) return res.status(400).json({ errorMessage: "Email already in Use"});

        const userPassword = req.body.password;
        const encryptedPassword = hashPassword(userPassword); 

        try {
            let newUser = new User();
            newUser.email = req.body.email;
            newUser.password = encryptedPassword;

            let result = await newUser.save();
        
            if(result) {
                sendVerificationMail(newUser.email);
                
                res.json({ message: "Account created, check your email to verify your account" });
                
            }
        } catch (e) {
            res.status(422).json({ errorMessage: "Something went wrong, please try again" });
        }
    });


    // '/lnqapi/v1/account/login - End point to log in a user by email [Auth Token not required: Generates Auth Token]
    api.post('/login', async (req, res) => {
        try {
            let result = await User.find({email: req.body.email});

            // console.log(result)

            if(result.length === 0) return res.status(401).json({ message: "Email Incorrect"});

            for (const user of result) {
                const passwordOnLogin = hashPassword(req.body.password);
                if(user.password !== passwordOnLogin) return res.status(401).json({ message: "Password Incorrect" });
                    
                if(user.verification.isVerified === false) res.status(401).json({ message: "Account Not Verified" });
                        
                let payload = {
                    id: user._id
                }
                
                let token = setToken(payload);
                
                res.json({
                    authenticated: true,
                    userId: user._id,
                    token: token
                });
                
            }
        } catch(e) {
            // console.log(e)
            res.status(400).json({ errorMessage: "Something went wrong, please try again later"});
        }
    });


    // '/lnqapi/v1/account/:id - End point to get the details of a user by id [ Requires Auth]
    
    api.get('/:id', async (req, res) => {

        let accessToken = req.body.token || req.query.token || req.headers['x-access-token'];

        if(!accessToken) return res.status(407).json({ message: "No token provided" });

        let isTokenValid = verifyToken(accessToken);

        // console.log(isTokenValid);
        
        if(isTokenValid === false) return res.status(407).json({ message: "Failed to verify token"});
    
        try {
           let result = await User.findById(req.params.id);

           if(result) res.json(result);
            
        } catch(e) {
            // console.log(e.name);
            res.status(400).json(e);
        }
    });

    // 'lnqapi/v1/account/:id' - Endpoint for updating the profile of a user [ Requires Auth]

    api.put('/:id', async (req, res) => {

        let accessToken = req.body.token || req.query.token || req.headers['x-access-token'];

        if(!accessToken) {
            return res.status(407).json({ message: "No token provided" });
        }

        let isTokenValid = verifyToken(accessToken);
        
        if(isTokenValid === false) {
            return res.status(401).json({ message: "Failed to verify token"});
        } 

        try {
            // console.log("gets here");
            let result = await User.findById(req.params.id);
            
            if(result) {
                result.profile.displayName = req.body.profile.displayName;
                result.profile.bio = req.body.profile.bio;
                result.profile.sex = req.body.profile.sex;
                result.profile.website = req.body.profile.website;
                result.profile.phoneNumber = req.body.profile.phoneNumber;
                result.profile.birthday = req.body.profile.birthDay;
            } else{
                return res.status(400).json({ message: "Something went wrong" });
            }
    
            let user = await result.save();

            if(user) {
                res.json({ message: "Info Updated Successfully" });
            }
        } catch(e) {
            res.status(407).json({ message: "User not found, info not updated" });
        }
    });

    // '/lnqapi/v1/account/changePassword/:id' - Endpoint for updating the password of a user [ Requires Auth ]

    api.put('/changePassword/:id', async (req, res) => {

        let accessToken = req.body.token || req.query.token || req.headers['x-access-token'];

        if(!accessToken) {
            return res.status(407).json({ message: "No token provided" });
        }

        let isTokenValid = verifyToken(accessToken);
        
        if(isTokenValid === false) {
            return res.status(401).json({ message: "Failed to verify token"});
        } 

        try {
            let result = await User.findById(req.params.id);
            
            if (result) {
                result.password = req.body.password;

                let user = await result.save();

                if (user) {
                    res.json({ message: "Password Updated Successfully" });
                }
            } else {
                res.json({ message: "User not found, could not change password" });
            }
        } catch(e) {
            res.status(407).json(e);
        }
    });

    // '/lnqapi/v1/account/deleteUser/:id' - Endpoint for deleting a user [ Requires Auth]

    api.delete('/deleteUser/:id', async (req, res) => {

        let accessToken = req.body.token || req.query.token || req.headers['x-access-token'];

        if(!accessToken) {
            return res.status(407).json({ message: "No token provided" });
        }

        let isTokenValid = verifyToken(accessToken);
        
        if(isTokenValid === false) {
            return res.status(401).json({ message: "Failed to verify token"});
        } 

        try {
            const tempUser = await User.findById(req.params.id);
            const user = await User.remove({ _id: req.params.id });

            if(user) {
                const dp = await displayPictureModel.remove({ _id: tempUser.profile.displayPicture});

                if(dp) {
                    res.json({ message: "User Account Deleted"});
                }
            }
        } catch (e) {
            res.status(400).json(e);
        }
    });

    return api;
}