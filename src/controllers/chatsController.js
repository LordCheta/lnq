import { Router } from 'express';
import User from '../models/userModel';
import displayPictureModel from '../models/displayPictureModel';
import Message from '../models/messagesModel';
// import io from 'socket.io';

export default ({ config, db }) => {

    let api = Router();

    // '/lnqapi/v1/chat/' - End point to chat
    api.get('/', async (req, res) => {
        // res.sendFile(__dirname + '/testUI/privateChat.html');
        // res.send("Works");
        let socket_id = [];
        let io = req.app.get('socketio');

        io.on('connection', (socket) => {
            socket_id.push(socket.id);
            // socket.broadcast.emit();
            
            if (socket_id[0] === socket.id) {
                // remove the connection listener for any subsequent 
                // connections with the same ID
                io.removeAllListeners('connection'); 
            }
            
            console.log('A user connected');

            socket.on('disconnect', () => {
                // console.log('A user disconnected');
            });

            socket.on('chatMessage', (msg) => {
                // socket.broadcast.emit(msg);
                console.log(msg);
                socket.broadcast.emit('resMessage', msg);
            });
        });
    });


    return api;
}